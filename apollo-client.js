// ./apollo-client.js

import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client"

const httpLink = new HttpLink({
  uri: "http://localhost:8000/",
})

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: httpLink,
})

export default client
