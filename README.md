This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev

```

Open [http://localhost:3000](http://localhost:3000) with your browser to see Deal Sasa's front page.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## About Deal Sasa

Deal Sasa is an E-commerce platform that serves customers with lastest deals and promotions across different shops.

Shop owners get to register and list their products on Deal Sasa's MongoDB database.

Customers on the other hand can explore these products and our user interface makes it easy to find the best deals of any product they wish.
