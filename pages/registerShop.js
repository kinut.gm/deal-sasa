import { Title } from "../components"
import { useState } from "react"
import { Button, Checkbox, Divider, Input, message } from "antd"
import { useRouter } from "next/router"

import { useMutation, gql, useQuery } from "@apollo/client"

const GET_SHOPNAMES = gql`
  query ShopMany {
    shopMany {
      email
      name
      _id
    }
  }
`
const ADD_SHOP = gql`
  mutation ShopCreateOne($record: CreateOneShopInput!) {
    shopCreateOne(record: $record) {
      record {
        name
        password
        _id
      }
    }
  }
`

export default function RegisterShop() {
  const [shopName, setShopName] = useState("")
  const [shopEmail, setShopEmail] = useState("")
  const [loadingRegister, setLoadingRegister] = useState(false)
  const [password, setPassword] = useState("")
  const [rPassword, setRpassword] = useState("")

  const router = useRouter()

  const handleRegister = () => {
    if (
      data.shopMany.filter(
        (shop) => shop.email.toLowerCase() === shopEmail.toLowerCase()
      ).length
    ) {
      message.info(
        "Oops! Shop with same email already exists. Log in with shop console option instead"
      )
    } else {
      if (password !== rPassword) {
        message.error("Passwords do not match")
      } else {
        _addShop({
          variables: {
            record: {
              name: shopName,
              password: rPassword,
            },
          },
        }).then((result) => {
          setPassword("")
          setShopName("")
          setRpassword("")
          setShopEmail("")
          message.success(
            "Welcome! Your shop is now set up. Heading to your console..."
          )

          localStorage.setItem("shopId", result.data.shopCreateOne.record._id)

          router.push("/shopConsole")
        })
      }
    }
  }

  const { loading, error, data } = useQuery(GET_SHOPNAMES)

  const [
    _addShop,
    { data: dataMutation, loading: loadingMutation, error: errorMutation },
  ] = useMutation(ADD_SHOP)

  if (loading) return <div>Loading...</div>
  if (error) return <div>Error!</div>

  console.log(data)

  return (
    <div style={{ padding: "6rem 2rem 2rem 2rem" }}>
      <Title large text="Register shop" />

      {/* <p style={{ color: "#707070" }}>
        Be a pro seller by joining other pro sellers.
      </p> */}
      <br />

      <Input
        style={{
          width: "100%",
          marginBottom: 12,
        }}
        placeholder="Shop name"
        type="text"
        value={shopName}
        onChange={(e) => setShopName(e.target.value)}
      />

      <Input
        style={{
          width: "100%",
          marginBottom: 12,
        }}
        placeholder="Shop email"
        type="text"
        value={shopEmail}
        onChange={(e) => setShopEmail(e.target.value)}
      />

      <Input.Password
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        placeholder="Password"
        style={{
          width: "100%",
          marginBottom: 12,
        }}
      />

      <Input.Password
        type="password"
        value={rPassword}
        onChange={(e) => setRpassword(e.target.value)}
        placeholder="Repeat password"
        style={{
          width: "100%",
          marginBottom: 12,
        }}
      />

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          marginTop: 12,
        }}
      >
        <Checkbox onChange={null}>
          <span style={{ color: "#909090" }}>Remember me</span>
        </Checkbox>
        <Button
          type="link"
          style={{ padding: 0, margin: 0 }}
          onClick={() =>
            message.info("Forgot password feature is awaiting implementation")
          }
        >
          Forgot password
        </Button>
      </div>

      <Button
        loading={loadingRegister}
        size="large"
        onClick={handleRegister}
        style={{
          fontFamily: "Metropolis-Regular",
          display: "block",
          fontSize: "1rem",
          backgroundColor: "#3F9B42",
          fontWeight: "900",
          textTransform: "uppercase",
          color: "#fff",
          border: "none",
          borderRadius: "8px",
          width: "100%",
          height: 36,
          marginTop: "2rem",
        }}
        full
      >
        {"Register"}
      </Button>

      <Divider>or</Divider>
      <br />
      <p style={{ color: "#909090", textAlign: "center" }}>
        Already a member.{" "}
        <Button
          type="link"
          onClick={() => router.push("/shopConsole")}
          style={{ padding: 0, margin: 0 }}
        >
          Go to console
        </Button>
      </p>
    </div>
  )
}
