import {
  Container,
  Product,
  Button,
  Flex,
  Page,
  SkeletonCustom,
} from "../../components"
import { Button as ButtonAntd, Divider, message } from "antd"
import { useRouter } from "next/router"
import { useState } from "react"

export default function Basket() {
  const router = useRouter()

  const [cart, setCart] = useState([])

  const products = [
    {
      name: "Dove body lotion - 500ml",
      category: "supermarket",
      shops: [
        {
          name: "Frandel Beauty",
          price: 600,
        },
        {
          name: "Glam Girl",
          price: 595,
        },
        {
          name: "Abiko Boutique",
          price: 630,
        },
      ],
      image: "/products/dove-body-lotion.jpg",
      id: 1,
    },
    {
      name: "Qwen flash drive - 64GB",
      category: "computing",
      shops: [
        {
          name: "Frandel Electronics",
          price: 1600,
        },
        {
          name: "Banda Computing ",
          price: 1400,
        },
        {
          name: "Blue Cloud Softwares",
          price: 2000,
        },
      ],
      image: "/products/flash-drive.jpg",
      id: 2,
    },
    {
      name: "14 - 42 inch TV mount",
      category: "electronics",
      shops: [
        {
          name: "Banda Computing",
          price: 450,
        },
        {
          name: "Blue Cloud Softwares",
          price: 250,
        },
        {
          name: "Mwamba Merchandise",
          price: 300,
        },
      ],
      image: "/products/mount.jpg",
      id: 3,
    },
    {
      name: "Sunlight Detergent - 500g",
      category: "supermarket",
      shops: [
        {
          name: "Naivas Supermarket",
          price: 185,
        },
        {
          name: "Quickmatt",
          price: 190,
        },
        {
          name: "Jumia",
          price: 200,
        },
      ],
      image: "/products/sunlight-detergent.jpg",
      id: 4,
    },
    {
      name: "Aramco water dispenser",
      category: "electronics",
      shops: [
        {
          name: "Naivas supermarket",
          price: 6000,
        },
        {
          name: "Delta Electronics",
          price: 5700,
        },
        {
          name: "Frandel Electronics",
          price: 6200,
        },
      ],
      image: "/products/water-dispenser.jpg",
      id: 5,
    },
    {
      name: "Nunix water dispenser",
      category: "electronics",
      shops: [
        {
          name: "Naivas supermarket",
          price: 8000,
        },
        {
          name: "Delta Electronics",
          price: 9500,
        },
        {
          name: "Frandel Electronics",
          price: 9700,
        },
      ],
      image: "/products/water-dispenser-2.jpg",
      id: 6,
    },
  ]

  const Bill = ({ total }) => {
    return (
      <p
        style={{
          color: "rgb(229, 224, 85)",
          lineHeight: 2.5,
          fontFamily: "Metropolis-Regular",
        }}
      >
        {total}
      </p>
    )
  }

  const getTotal = () => {
    let tot = 0
    products
      .filter((product) =>
        localStorage.getItem("cart").split(",").includes(product.id.toString())
      )
      .forEach((product) => {
        tot += product.shops[0].price * 1
      })
    return tot
  }

  const increment = (id) => {
    console.log("increment")
  }

  const decrement = (id, name) => {
    console.log("decrement")
  }

  const removeFromCart = (id, name) => {
    console.log("remove")
  }

  return (
    <Page title="Basket" extra={<Bill total={`Ksh. ${getTotal()}`} />}>
      <Container style={{ position: "absolute", top: 56, minWidth: "95vw" }}>
        <Flex scrollY>
          {products
            .filter((product) =>
              localStorage
                .getItem("cart")
                .split(",")
                .includes(product.id.toString())
            )
            .map((product) => (
              <Product
                on_offer={false}
                offer_price={0}
                key={product.id}
                image={product.image}
                add={() => addToCart(product)}
                rounded
                basket={true}
                quantity={0}
                name={product.name}
                price={product.shops[0].price}
                shop={product.shops[0].name}
                id={product.id}
                product={product}
                remove={(id) => {
                  console.log("remove", id)
                }}
                increment={null}
                decrement={(id) => console.log("decrement", id)}
              />
            ))}
        </Flex>

        <Button
          onClick={() => router.push("/checkout")}
          rounded
          label="Checkout"
          yellow
          width="90%"
          large
          style={{ position: "fixed", bottom: "3rem" }}
        />

        {localStorage.getItem("cart").split(",").length == 0 && (
          <>
            <img
              src="https://img.icons8.com/dusk/100/000000/shopping-basket-2.png"
              style={{
                position: "absolute",
                top: "20vh",
                left: "50%",
                transform: "translateX(calc(-50% + 8px))",
              }}
            />
            <p
              style={{
                color: "#3F9B42",
                position: "absolute",
                transform: "translateX(calc(-50% + 8px))",
                letterSpacing: "-0.05rem",
                top: "calc(20vh + 120px)",
                left: "50%",
                fontSize: "1.5rem",
              }}
            >
              Basket empty!
            </p>
            <ButtonAntd
              style={{
                width: "100%",
                color: "black",
                position: "absolute",
                transform: "translateX(calc(-50% + 8px))",
                top: "calc(20vh + 180px)",
                left: "50%",
              }}
              onClick={() => router.push("/")}
              type="link"
            >
              <p style={{ textDecoration: "underline", fontWeight: "400" }}>
                {" "}
                Start shopping
              </p>
            </ButtonAntd>
          </>
        )}
      </Container>
    </Page>
  )
}
