import {
  Container,
  Product,
  Button,
  Title,
  Search,
  Category,
  Text,
  Flex,
  Page,
} from "../../components";
import { useRouter } from "next/router";

export default function Response() {
  const router = useRouter();

  const { response } = router.query;

  if (response === "success") return <Success />;
  if (response === "error") return <Error />;

  return <p>Loading</p>;
}

const Success = () => {
  return (
    <Page>
      <div
        style={{
          width: "80%",
          display: "block",
          margin: "0 auto",
          marginTop: "20vh",
        }}
      >
        <img />
        <Title center text="Success!" />
        <br />
        <Text
          center
          text="Order complete. We are packing your items. We'll deliver them shortly. Stay alert for our call."
        />
      </div>
      <Button
        rounded
        large
        onClick={() => (window.location = "/")}
        label="Go to home"
        width="90%"
        style={{ marginLeft: "5%", position: "absolute", bottom: "3rem" }}
      />
    </Page>
  );
};

const Error = () => {
  return (
    <Page>
      <div
        style={{
          width: "80%",
          display: "block",
          margin: "0 auto",
          marginTop: "20vh",
        }}
      >
        <img />
        <Title yellow center text="Oops!" />
        <br />
        <Text
          center
          text="We are sorry ! Order not complete. Something gross happened. Try again at a later time."
        />
      </div>
      <Button
        rounded
        large
        onClick={() => (window.location = "/")}
        yellow
        label="Go to home"
        width="90%"
        style={{ marginLeft: "5%", position: "absolute", bottom: "3rem" }}
      />
    </Page>
  );
};
