import "../styles/globals.css"
import "antd/dist/antd.css"
import { SessionProvider } from "next-auth/react"

import NextNProgress from "nextjs-progressbar"

import { ApolloProvider } from "@apollo/client"
import client from "../apollo-client"

function MyApp({ Component, pageProps }) {
  return (
    <SessionProvider session={pageProps.session}>
      <ApolloProvider client={client}>
        <NextNProgress color="#3F9B42" />
        <Component {...pageProps} />
      </ApolloProvider>
    </SessionProvider>
  )
}

export default MyApp
