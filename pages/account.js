import { Page, Container, Button } from "../components";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import {
  Image,
  Avatar,
  Form,
  Input,
  message,
  Skeleton,
  Typography,
} from "antd";
import {
  UserOutlined,
  HighlightOutlined,
  InfoCircleOutlined,
} from "@ant-design/icons/lib/icons";
import { useAuth } from "../context";
import { db } from "../firebase/clientApp";
import DB_Services from "../firebase/services";

import { doc } from "firebase/firestore";
import { useDocument } from "react-firebase-hooks/firestore";
import Login from "../pages/login";

const { Paragraph } = Typography;

export default function Account() {
  const router = useRouter();
  const { authUser, loading, _signOut } = useAuth();

  const [name, setName] = useState("");
  const [uda, setUDA] = useState(false);
  const [email, setEmail] = useState("");
  const [location, setLocation] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [showLogin, setShowLogin] = useState(false);

  const [user, userLoading, userError] = useDocument(
    doc(db, "users", authUser ? authUser.uid : "unfindalble")
  );

  useEffect(() => {
    if (user && typeof user.data() !== "undefined") {
      setUDA(true);
    }
  }, [user]);

  useEffect(() => {
    if (uda && name == user.data().displayName) {
      setName("");
    }
    if (uda && phoneNumber == user.data().phoneNumber) {
      setPhoneNumber("");
    }
    if (uda && email == user.data().email) {
      setEmail("");
    }
    if (uda && location == user.data().location) {
      setLocation("");
    }
  }, [name, phoneNumber, email, location]);

  const updateProfile = () => {
    let update = {};
    if (name !== "") {
      update.displayName = name;
    }
    if (email !== "") {
      update.email = email;
    }
    if (phoneNumber !== "") {
      update.phoneNumber = phoneNumber;
    }
    if (location !== "") {
      update.location = location;
    }

    DB_Services.updateUser(update, authUser.uid)
      .then(() => message.success("Profile updated!"))
      .catch((err) => message.error("Failed to update profile"));
  };

  const Logout = () => {
    return (
      <button
        onClick={() => {
          _signOut().then((res) => {
            if (res) {
              message.info("Logged out").then(() => setShowLogin(true));
            }
          });
        }}
        style={{
          border: "none",
          outline: "none",
          background: "transparent",
          color: "#E5E055",
          fontSize: "0.9rem",
          zIndex: 4,
        }}
      >
        Sign out
      </button>
    );
  };

  if (showLogin) return <Login />;

  if (authUser == null) return <Login />;

  return (
    <Page title="Account" extra={<Logout />}>
      <Container style={{ position: "absolute", top: 56, width: "95%" }}>
        <br />

        <Form style={{ marginBottom: "7rem" }}>
          <Form.Item label="Name" name="name">
            {uda ? (
              <div style={{ padding: "4px 0px" }}>
                <input
                  style={{
                    background: "#f1f1f1",
                    width: "100%",
                    padding: "14px",
                    border: "none",
                    outline: "none",
                    borderRadius: 12,
                    color: name ? "#E5E055" : "#3F9B42",
                    fontWeight: "bold",
                    fontSize: "1.1rem",
                  }}
                  type="text"
                  value={name ? name : user.data().displayName}
                  onChange={(e) => setName(e.target.value)}
                />
              </div>
            ) : (
              <Skeleton.Button active block size="large" />
            )}
          </Form.Item>
          <Form.Item label="Phone number" name="telephone">
            {uda ? (
              <div style={{ padding: "4px 0px" }}>
                <input
                  style={{
                    background: "#f1f1f1",
                    width: "100%",
                    padding: "14px",
                    border: "none",
                    outline: "none",
                    borderRadius: 12,
                    color: phoneNumber ? "#E5E055" : "#3F9B42",
                    fontWeight: "bold",
                    fontSize: "1.1rem",
                  }}
                  type="number"
                  value={
                    phoneNumber
                      ? phoneNumber
                      : user.data().phoneNumber
                      ? user.data().phoneNumber
                      : ""
                  }
                  onChange={(e) => setPhoneNumber(e.target.value)}
                />
              </div>
            ) : (
              <Skeleton.Button active block size="large" />
            )}
          </Form.Item>
          <Form.Item label="Email" name="email">
            {uda ? (
              <div style={{ padding: "4px 0px" }}>
                <input
                  style={{
                    background: "#f1f1f1",
                    width: "100%",
                    padding: "14px",
                    border: "none",
                    outline: "none",
                    borderRadius: 12,
                    color: email ? "#E5E055" : "#3F9B42",
                    fontWeight: "bold",
                    fontSize: "1.1rem",
                  }}
                  type="text"
                  value={
                    email ? email : user.data().email ? user.data().email : ""
                  }
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
            ) : (
              <Skeleton.Button active block size="large" />
            )}
          </Form.Item>
          <span>
            <InfoCircleOutlined style={{ marginRight: 8, color: "green" }} />
            <Paragraph
              style={{ display: "inline", fontSize: "0.8rem", color: "green" }}
            >
              * Building name, house no.
            </Paragraph>
          </span>
          <Form.Item label="Location" name="address" style={{ marginTop: 16 }}>
            {uda ? (
              <div style={{ padding: "4px 0px" }}>
                <input
                  style={{
                    background: "#f1f1f1",
                    width: "100%",
                    padding: "14px",
                    border: "none",
                    outline: "none",
                    borderRadius: 12,
                    color: location ? "#E5E055" : "#3F9B42",
                    fontWeight: "bold",
                    fontSize: "1.1rem",
                  }}
                  type="text"
                  value={
                    location
                      ? location
                      : user.data().location
                      ? user.data().location
                      : ""
                  }
                  onChange={(e) => setLocation(e.target.value)}
                />
              </div>
            ) : (
              <Skeleton.Button active block size="large" />
            )}
          </Form.Item>
        </Form>
        <Button
          rounded
          label="Update"
          yellow
          width="90%"
          large
          style={{ position: "fixed", bottom: "3rem" }}
          onClick={updateProfile}
        />
      </Container>
    </Page>
  );
}
