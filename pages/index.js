import { useState, useRef } from "react"
import { useSession, signIn, signOut } from "next-auth/react"

import { useQuery, gql } from "@apollo/client"
import {
  Container,
  Product,
  Title,
  Search,
  Category,
  Flex,
  SkeletonCustom,
} from "../components"

import {
  Badge,
  Avatar,
  Carousel,
  message,
  Dropdown,
  Skeleton,
  Spin,
} from "antd"
import { ShoppingOutlined, UserOutlined } from "@ant-design/icons/lib/icons"

import { useRouter } from "next/router"

const GET_PRODUCTS = gql`
  query ProductsPopulate {
    productsPopulate {
      name
      shops {
        name
        price
        id
      }
      category
      id
    }
  }
`

const GET_CART_COUNT = gql`
  query Query($filter: FilterCountCartInput) {
    cartCount(filter: $filter)
  }
`

export default function Home() {
  const router = useRouter()
  const { data: session } = useSession()

  console.log(session)

  const [highlighted, setHighlighted] = useState("")
  const [keyword, setKeyword] = useState("")
  const [id, setId] = useState(null)
  const [loadingMore, setLoadingMore] = useState(false)

  const scrollDiv = useRef()

  const { loading, error, data } = useQuery(GET_PRODUCTS)

  const {
    loading: loadingCount,
    error: errorCount,
    data: dataCount,
  } = useQuery(GET_CART_COUNT, {
    variables: {
      filter: {
        customerId: "62500b76af64fd2326efdb3a",
      },
    },
  })

  const results = (
    <div
      style={{ maxHeight: 200, width: "97%", background: "#fff" }}
      id="search-node"
    >
      {data &&
        data.productsPopulate
          .filter((product) =>
            product.name.toLowerCase().includes(keyword.toLowerCase())
          )
          .map((product) => (
            <a
              href={`#${product.id}`}
              onClick={() => {
                setHighlighted(product.id)
                setKeyword("")
              }}
              key={product.id}
              style={{ display: "block", margin: "12px", width: "100%" }}
            >
              {product.name}
            </a>
          ))}
      {!data && [1, 2, 3, 4, 5].map((el) => <Skeleton.Button key={el} />)}
    </div>
  )
  if (loading) return <div>Loading...</div>
  if (error) return <div>Error!</div>

  console.log(data)

  return (
    <Container>
      {/* Header */}
      <>
        <div
          style={{
            background: "#fff",
            display: "flex",
            position: "fixed",
            display: "flex",
            top: 0,
            width: "100%",
            padding: "12px 0px",
            zIndex: 3,
          }}
        >
          <Dropdown
            overlay={results}
            placement="bottomCenter"
            visible={keyword ? true : false}
          >
            <Search
              rounded
              placeholder="Search eggs , ketchup..."
              value={keyword}
              onChange={(e) => setKeyword(e.target.value)}
            />
          </Dropdown>

          <div style={{ height: "inherit", paddingTop: 6, display: "flex" }}>
            <button
              style={{
                height: 40,
                margin: 0,
                outline: "none",
                border: "none",
                background: "white",
                display: "inline",
                top: 12,
              }}
              onClick={() => {
                router.push("/basket")
              }}
            >
              <Badge
                color="#3F9B42"
                count={dataCount ? dataCount.cartCount : 0}
              >
                <ShoppingOutlined
                  style={{ fontSize: "1.5rem", color: "#707070" }}
                />
              </Badge>
            </button>
            <button
              style={{
                height: 40,
                margin: 0,
                outline: "none",
                border: "none",
                background: "white",
                display: "inline",
                top: 12,
              }}
              onClick={() => {
                router.push("/login")
              }}
            >
              <Avatar
                style={{ backgroundColor: "#3F9B42" }}
                icon={<UserOutlined />}
              />
            </button>
          </div>
        </div>
        <br />
      </>

      <div
        ref={scrollDiv}
        onScroll={() => console.log("Scrolled")}
        style={{
          position: "absolute",
          top: 78,
          width: "100%",
          maxHeight: "200vh",
          overflow: "auto",
        }}
      >
        <Carousel autoplay>
          <div style={{ height: 160, width: "calc(100vw - 32px)" }}>
            <img
              style={{ width: "100%", objectFit: "cover" }}
              src="/promos/nivea.jpg"
            />
          </div>
          <div style={{ width: "calc(100vw - 32px)" }}>
            <img
              style={{ width: "100%", objectFit: "cover" }}
              src="/promos/promo2.jpg"
            />
          </div>
          <div style={{ width: "calc(100vw - 32px)" }}>
            <img
              style={{ width: "100%", objectFit: "cover" }}
              src="/promos/promo3.jpg"
            />
          </div>
        </Carousel>

        {/* Categories */}
        <>
          <Title text="Categories" />
          <br />

          <div style={{ display: "flex", maxWidth: "100%", flexWrap: "wrap" }}>
            {[
              "supermarket",
              "computing",
              "electronics",
              "fashion",
              "sporting",
            ].map((cat, inx) => (
              <Category
                key={inx}
                icon={
                  cat == "supermarket"
                    ? "/categories/market.svg"
                    : cat == "computing"
                    ? "/categories/computing.svg"
                    : cat == "electronics"
                    ? "/categories/power.svg"
                    : cat == "fashion"
                    ? "/categories/clothing.png"
                    : cat == "sporting"
                    ? "/categories/sport.svg"
                    : "/categories/hardware.png"
                }
                rounded
                label={
                  cat.charAt(0).toUpperCase() + cat.substring(1, cat.length)
                }
              />
            ))}
          </div>
          <br />
        </>

        {/* Recommended */}
        <>
          <Title text="Recommended" />
          <br />

          {data.productsPopulate.length < 0 && (
            <Flex scrollY>
              {[1, 2, 3, 5, 6, 7, 8, 9, 4].map((el) => (
                <SkeletonCustom key={el} />
              ))}
            </Flex>
          )}

          <Flex scrollY>
            {data.productsPopulate
              .filter((product) =>
                product.name.toLowerCase().includes(keyword.toLowerCase())
              )
              .map((product) => (
                <Product
                  on_offer={false}
                  offer_price={0}
                  key={product.id}
                  image={null}
                  loading={false}
                  add={() => {
                    console.log("add to cart")
                  }}
                  rounded
                  highlighted={highlighted == product.id ? true : false}
                  basket={false}
                  quantity={0}
                  shop={product.shops[product.shops.length - 1].name}
                  price={product.shops[product.shops.length - 1].price}
                  name={product.name}
                  id={product.id}
                  product={product}
                  remove={(id) => {
                    console.log("remove", id)
                  }}
                  increment={(id) => console.log("increment", id)}
                  decrement={(id) => console.log("decrement", id)}
                />
              ))}
          </Flex>

          {loadingMore && (
            <Spin
              style={{
                position: "absolute",
                bottom: "0.5rem",
                left: "50%",
                transform: "translateY(-50%)",
              }}
            />
          )}
        </>
      </div>
    </Container>
  )
}
