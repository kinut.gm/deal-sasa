import {
  Container,
  Product,
  Button,
  Flex,
  Page,
  Row,
  Col,
  SkeletonCustom,
} from "../components"
import {
  Button as ButtonAntd,
  Divider,
  message,
  Image,
  Skeleton,
  Typography,
} from "antd"
import { PlusOutlined, MinusOutlined, DeleteOutlined } from "@ant-design/icons"
import { useRouter } from "next/router"
import { useQuery, gql, useMutation } from "@apollo/client"

const { Text } = Typography

const GET_CART = gql`
  query CartPopulate($customer: ID) {
    cartPopulate(customer: $customer) {
      id
      shop {
        name
        id
      }
      product {
        name
        id
      }
      quantity
      insertionDate
      price
      orderDate
    }
  }
`

const REMOVE_FROM_CART = gql`
  mutation CartRemoveOne($filter: FilterRemoveOneCartInput) {
    cartRemoveOne(filter: $filter) {
      record {
        product {
          name
        }
        shop {
          name
        }
        customer {
          name
        }
      }
    }
  }
`

export default function Basket() {
  const { loading, error, data } = useQuery(GET_CART, {
    variables: {
      customer: "62500b76af64fd2326efdb3a",
    },
  })

  const [
    _removeFromCart,
    { data: dataMutation, loading: loadingMutation, error: errorMutation },
  ] = useMutation(REMOVE_FROM_CART)

  const router = useRouter()

  const Bill = ({ total }) => {
    return (
      <p
        style={{
          color: "rgb(229, 224, 85)",
          lineHeight: 2.5,
          fontFamily: "Metropolis-Regular",
        }}
      >
        {total}
      </p>
    )
  }

  const getTotal = () => {
    let tot = 0
    data.cartPopulate.forEach((cartItem) => {
      if (cartItem.orderDate == null) {
        tot += cartItem.quantity * cartItem.price
      }
    })
    return tot
  }

  const increment = (id) => {
    console.log("increment")
  }

  const decrement = (id, name) => {
    console.log("decrement")
  }

  const handleRemoveFromCart = (id) => {
    console.log(id)
    _removeFromCart({
      variables: {
        filter: {
          _id: id,
        },
      },
    })
      .then(() => {
        message.success("Product removed from basket successfully")
        router.reload(window.location.pathname)
      })
      .catch((err) => message.error(err.message))
  }

  if (loading) return <div>Loading...</div>
  if (error) return <div>Error!</div>

  return (
    <Page title="Basket" extra={<Bill total={`Ksh. ${getTotal()}`} />}>
      <Container style={{ position: "absolute", top: 56, minWidth: "95vw" }}>
        <Flex scrollY>
          {data.cartPopulate.map((cartItem) => {
            return (
              <>
                <div
                  style={{
                    display: "flex",
                    position: "relative",
                    width: "95%",
                    margin: "0px 0px 16px 0px",
                  }}
                >
                  <div style={{ width: 110 }}>
                    {" "}
                    <Skeleton.Button
                      style={{
                        width: 90,
                        height: 90,
                      }}
                      active
                    />
                  </div>
                  <div style={{ width: "50vw" }}>
                    <Text
                      ellipsis
                      style={{ margin: 0, padding: 0, width: "100%" }}
                    >
                      {cartItem.product.name}
                    </Text>
                    <Text
                      ellipsis
                      style={{
                        margin: "6px 0px",
                        color: "#808080",
                        fontSize: "0.9rem",
                        width: "90%",
                      }}
                    >
                      {cartItem.shop.name}
                    </Text>
                    <p style={{ color: "#3F9B42" }}>KES {cartItem.price}</p>
                    <div style={{ display: "flex" }}>
                      <Button
                        rounded
                        label={"-"}
                        style={{ height: 30, width: 30, padding: 0 }}
                        full
                        onClick={() =>
                          message.info(
                            "Decrement feature still a work in progress"
                          )
                        }
                      />
                      <p style={{ margin: "0px 16px", lineHeight: 2.5 }}>
                        {cartItem.quantity}
                      </p>
                      <Button
                        rounded
                        label={"+"}
                        style={{ height: 30, width: 30, padding: 0 }}
                        full
                        onClick={() =>
                          message.info(
                            "Increment feature still a work in progress"
                          )
                        }
                      />
                    </div>
                  </div>
                  <div style={{ position: "absolute", right: 0 }}>
                    <Button
                      rounded
                      label={<DeleteOutlined />}
                      style={{ height: 36, width: 36, background: "#3F9B42" }}
                      full
                      onClick={() => {
                        handleRemoveFromCart(cartItem.id)
                      }}
                    />
                  </div>
                </div>
                <Divider style={{ margin: 0, marginBottom: 16 }} />
              </>
            )
          })}
        </Flex>
        <Button
          onClick={() =>
            message.info("Checkout feature still a work in progress")
          }
          rounded
          label="Checkout"
          yellow
          width="90%"
          large
          style={{ position: "fixed", bottom: "3rem" }}
        />

        {data.cartPopulate.length < 1 && (
          <>
            <img
              src="https://img.icons8.com/dusk/100/000000/shopping-basket-2.png"
              style={{
                position: "absolute",
                top: "20vh",
                left: "50%",
                transform: "translateX(calc(-50% + 8px))",
              }}
            />
            <p
              style={{
                color: "#3F9B42",
                position: "absolute",
                transform: "translateX(calc(-50% + 8px))",
                letterSpacing: "-0.05rem",
                top: "calc(20vh + 120px)",
                left: "50%",
                fontSize: "1.5rem",
              }}
            >
              Basket empty!
            </p>
            <ButtonAntd
              style={{
                width: "100%",
                color: "black",
                position: "absolute",
                transform: "translateX(calc(-50% + 8px))",
                top: "calc(20vh + 180px)",
                left: "50%",
              }}
              onClick={() => router.push("/")}
              type="link"
            >
              <p style={{ textDecoration: "underline", fontWeight: "400" }}>
                {" "}
                Start shopping
              </p>
            </ButtonAntd>
          </>
        )}
      </Container>
    </Page>
  )
}
