import {
  Button,
  Divider,
  Tabs,
  Typography,
  Image,
  Tag,
  Empty,
  Input,
  message,
  Modal,
  Dropdown,
  Select,
  Menu,
  Checkbox,
  Row,
  Col,
} from "antd"
import { useState } from "react"
import { Title } from "../components"
import { gql, useQuery, useMutation } from "@apollo/client"

import { PlusOutlined, SelectOutlined } from "@ant-design/icons"
import { useRouter } from "next/router"

const { Paragraph } = Typography
const { Option } = Select

const GET_SHOPDETAILS = gql`
  query ShopPopulate($shopPopulateId: ID) {
    shopPopulate(id: $shopPopulateId) {
      id
      name
      email
      password
      products {
        name
        price
        id
      }
      phoneNumber
    }
  }
`
const EDIT_PRICE = gql`
  mutation ProductUpdatePrice($shopId: ID, $price: Float, $productId: ID) {
    productUpdatePrice(shopId: $shopId, price: $price, productId: $productId)
  }
`

const REMOVE_PRODUCT = gql`
  mutation ProductDelete($shopId: ID, $productId: ID) {
    productDelete(shopId: $shopId, productId: $productId)
  }
`

const ADD_PRODUCT = gql`
  mutation ShopAddProduct(
    $shopId: ID
    $price: Float
    $productId: ID
    $name: String
    $category: String
  ) {
    shopAddProduct(
      shopId: $shopId
      price: $price
      productId: $productId
      name: $name
      category: $category
    ) {
      name
      shopsMeta {
        shop {
          name
        }
        price
      }
      category
    }
  }
`

const GET_PRODUCTS = gql`
  query ProductMany {
    productMany {
      name
      _id
      category
    }
  }
`

export default function shopConsole() {
  const router = useRouter()
  let shopId

  if (typeof window !== "undefined") {
    shopId = window.localStorage.getItem("shopId")
  }

  const { loading, error, data } = useQuery(GET_SHOPDETAILS, {
    variables: {
      shopPopulateId: shopId,
    },
  })

  const { TabPane } = Tabs

  if (loading) return <div>Loading...</div>
  if (error) return <div>Error!</div>

  return (
    <div>
      <div
        style={{
          position: "fixed",
          display: "flex",
          top: 0,
          padding: "36px 8px 12px 36px",
          background: "white",
          width: "100%",
          justifyContent: "space-between",
          zIndex: 2,
        }}
      >
        <div>
          <p style={{ color: "#707070", padding: 0, margin: 0 }}>WELCOME</p>
          <Title text={data.shopPopulate.name} />
        </div>
        <div>
          <Button
            type="secondary"
            onClick={() => {
              localStorage.clear()
              message.info("Successfully logged out!")
              router.push("/login")
            }}
          >
            Sign out
          </Button>

          <Button
            type="link"
            onClick={() => {
              router.push("/")
            }}
            style={{ display: "block" }}
          >
            Homepage
          </Button>
        </div>
      </div>

      <div
        style={{
          marginTop: 100,
          padding: 24,
          position: "fixed",
          width: "100%",
        }}
      >
        <Tabs defaultActiveKey="1">
          <TabPane tab="Products" key="3">
            <Products data={data} shop={shopId} />
          </TabPane>
          <TabPane tab="Account" key="4">
            <Admin data={data} />
          </TabPane>
        </Tabs>
      </div>
    </div>
  )
}

const Editable = ({ label, setEditableStr, editableStr }) => {
  return (
    <div>
      <span style={{ color: "#3F9B42" }}>{label}</span>
      <Paragraph
        editable={{ onChange: setEditableStr }}
        style={{ margin: "8px 0px" }}
      >
        {editableStr}
      </Paragraph>
    </div>
  )
}

const Admin = ({ data }) => {
  const [shopName, setShopName] = useState(data.shopPopulate.name)
  const [shopEmail, setShopEmail] = useState(data.shopPopulate.email)
  const [shopPhone, setShopPhone] = useState(data.shopPopulate.phoneNumber)
  return (
    <div>
      <p style={{ color: "#909090" }}>
        Your shop ID is{" "}
        <span style={{ color: "#3F9B42" }}>{data.shopPopulate.id}</span>
      </p>
      <br />
      <Editable
        label="Shop name"
        setEditableStr={setShopName}
        editableStr={shopName}
      />
      <br />
      <Editable
        label="Shop/Owner email"
        setEditableStr={setShopEmail}
        editableStr={shopEmail}
      />
      <br />
      <Editable
        label="Shop/Owner phone"
        setEditableStr={setShopPhone}
        editableStr={shopPhone}
      />
      <br />
      <Button type="link" style={{ textAlign: "center", width: "100%" }}>
        Change password
      </Button>
    </div>
  )
}

// const Order = () => {
//   return (
//     <div
//       style={{
//         display: "flex",
//         background: "#f2f2f2",
//         padding: 4,
//         marginBottom: 4,
//       }}
//     >
//       <Image height={56} width={56} src={"/products/mount.jpg"} />
//       <div style={{ marginLeft: 12, padding: "8px 0px" }}>
//         <p style={{ color: "#3F9B42", padding: 0, margin: 0 }}>
//           [Product name]
//         </p>
//         <p style={{ padding: 0, margin: 0, color: "#909090" }}>
//           Ksh 320{" "}
//           <span
//             style={{
//               fontSize: "2rem",
//               margin: 0,
//               padding: 0,
//               lineHeight: 0.2,
//               margin: "0px 4px",
//             }}
//           >
//             .
//           </span>
//           <span>
//             <Tag color="black">3</Tag>
//           </span>
//           <span
//             style={{
//               fontSize: "2rem",
//               margin: 0,
//               padding: 0,
//               lineHeight: 0.2,
//               margin: "0px 4px",
//             }}
//           >
//             .
//           </span>
//           <span>{new Date().toDateString()}</span>
//         </p>
//       </div>
//     </div>
//   )
// }

// const Orders = () => {
//   const [orders, setOrders] = useState(false)
//   return (
//     <div
//       style={{
//         maxHeight: "calc(100vh - 170px)",
//         overflow: "scroll",
//       }}
//     >
//       {orders ? (
//         [1, 1, 1, 1, 1, 1].map((el) => <Order key={el} />)
//       ) : (
//         <Empty
//           image={Empty.PRESENTED_IMAGE_SIMPLE}
//           description="No orders yet"
//         />
//       )}
//     </div>
//   )
// }

// const Sales = () => {
//   const [sales, setSales] = useState(true)
//   return (
//     <div
//       style={{
//         maxHeight: "calc(100vh - 170px)",
//         overflow: "scroll",
//       }}
//     >
//       {sales ? (
//         [1, 1, 1, 1, 1, 1].map((el) => <Order key={el} />)
//       ) : (
//         <Empty
//           image={Empty.PRESENTED_IMAGE_SIMPLE}
//           description="No sales yet"
//         />
//       )}
//     </div>
//   )
// }

const Products = ({ data, shop }) => {
  const router = useRouter()

  const [
    _editPrice,
    { data: dataMutation, loading: loadingMutation, error: errorMutation },
  ] = useMutation(EDIT_PRICE)

  const [
    _removeProduct,
    { data: dataMutation2, loading: loadingMutation2, error: errorMutation2 },
  ] = useMutation(REMOVE_PRODUCT)

  const [
    _addProduct,
    { data: dataMutation3, loading: loadingMutation3, error: errorMutation3 },
  ] = useMutation(ADD_PRODUCT)

  const { loading, error, data: productsData } = useQuery(GET_PRODUCTS)

  const [modalVisible, setModalVisible] = useState(false)
  const [modal2Visible, setModal2Visible] = useState(false)
  const [id, setId] = useState("")
  const [newPrice, setNewPrice] = useState(null)

  const [name, setName] = useState("")
  const [nameNew, setNameNew] = useState("")
  const [category, setCategory] = useState("supermarket")
  const [price, setPrice] = useState(null)
  const [selectedId, setSelectedId] = useState("")
  const [inputDisabled, setInputDisabled] = useState(false)
  const [showSuggestions, setShowSuggestions] = useState(false)

  const handleEditPrice = () => {
    if (newPrice !== ("" || null)) {
      let variables = {
        shopId: data.shopPopulate.id,
        price: parseFloat(newPrice),
        productId: id,
      }
      console.log(variables)
      _editPrice({
        variables,
      })
        .then(() => {
          message.success("Price updated successfully")
          setModalVisible(false)
          setNewPrice(null)
          router.reload(window.location.pathname)
        })
        .catch((err) => {
          message.error("Price update failed")
          router.reload(window.location.pathname)
        })
    } else {
      message.warn("Input empty")
    }
  }

  const handleDelete = (productId) => {
    _removeProduct({
      variables: {
        shopId: data.shopPopulate.id,
        productId,
      },
    }).then(() => {
      message.success("Product removed successfully")
      router.reload(window.location.pathname)
    })
  }

  const handleAddProduct = () => {
    let payload = {
      id: selectedId,
      name,
      category,
      price,
    }
    if (name == "" || category == "" || price == ("" || null)) {
      message.warn("Missing fields")
    } else {
      _addProduct({
        variables: {
          shopId: shop,
          price: parseFloat(payload.price),
          productId: payload.id,
          name: payload.name,
          category: payload.category,
        },
      }).then(() => {
        message.success("Product added successfully")
        setModal2Visible(false)
        setName("")
        setPrice(null)
        setCategory("supermarket")
        setSelectedId("")
        router.reload(window.location.pathname)
      })
    }
  }

  console.log(productsData)

  const productSuggestions = (
    <Menu style={{ maxHeight: "50vh" }}>
      <p style={{ padding: 12, color: "#3F9B42" }}>Listed products</p>

      <div style={{ maxHeight: "30vh", overflow: "scroll" }}>
        {productsData &&
          productsData.productMany
            .filter((product) =>
              product.name.toLowerCase().includes(name.toLowerCase())
            )
            .map((product) => (
              <Row
                style={{
                  background: selectedId == product._id && "#3F9B42",
                  padding: 16,
                }}
                key={product._id}
                onClick={() => {
                  if (selectedId == "") {
                    setSelectedId(product._id)
                    setName(product.name)
                    setCategory(product.category)
                    setInputDisabled(true)
                    setShowSuggestions(false)
                  } else if (selectedId == product._id) {
                    setSelectedId("")
                    setName("")
                    setCategory("")
                    setInputDisabled(false)
                    setShowSuggestions(false)
                  } else {
                    setSelectedId(product._id)
                    setName(product.name)
                    setCategory(product.category)
                    setInputDisabled(true)
                    setShowSuggestions(false)
                  }
                }}
              >
                <Col span={23}>{product.name}</Col>
                {selectedId == product._id && (
                  <Col span={1}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                    >
                      <path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-1.959 17l-4.5-4.319 1.395-1.435 3.08 2.937 7.021-7.183 1.422 1.409-8.418 8.591z" />
                    </svg>
                  </Col>
                )}
              </Row>
            ))}
      </div>

      <div style={{ display: "flex", width: "100%", padding: "24px 12px" }}>
        <Button
          type="secondary"
          style={{
            width: "40%",
            height: 32,
            float: "right",
            fontSize: "0.8rem",
          }}
          onClick={() => {
            setShowSuggestions(false)
          }}
        >
          + New product
        </Button>
      </div>
    </Menu>
  )

  return (
    <div style={{ minHeight: "75vh" }}>
      {data.shopPopulate.products.length < 1 && (
        <p>No products available yet</p>
      )}

      {data.shopPopulate.products.map((product) => (
        <>
          <div
            style={{
              display: "flex",
              position: "relative",
              width: "95%",
              margin: "0px 0px 16px 0px",
            }}
          >
            <div style={{ width: 90 }}>
              <Image
                style={{
                  width: 72,
                  height: 72,
                  objectFit: "cover",
                  marginBottom: 12,
                  width: "100%",
                  maxWidth: "calc(200px - 24px)",
                  maxHeight: "calc(200px - 24px)",
                }}
                src={"/products/mount.jpg"}
                loading="eager"
              />
            </div>
            <div>
              <p style={{ margin: 0, padding: 0 }}>{product.name}</p>
              <p style={{ color: "#3F9B42" }}>KES {product.price}</p>
              <div style={{ display: "flex" }}>
                <Button
                  type="secondary"
                  onClick={() => {
                    setId(product.id)
                    setModalVisible(true)
                  }}
                >
                  Edit price
                </Button>
                <Button
                  type="link"
                  onClick={() => {
                    handleDelete(product.id)
                  }}
                >
                  Delete
                </Button>
              </div>
            </div>
          </div>
          <Divider style={{ margin: 0, marginBottom: 16 }} />
        </>
      ))}

      <Button
        type="primary"
        style={{
          width: 50,
          height: 50,
          borderRadius: "50%",
          position: "absolute",
          bottom: "2rem",
          right: "1rem",
        }}
        onClick={() => setModal2Visible(true)}
      >
        <PlusOutlined style={{ fontSize: "1.2rem" }} />
      </Button>

      <Modal
        okText="Add"
        onOk={handleAddProduct}
        onCancel={() => {
          setModal2Visible(false)
        }}
        visible={modal2Visible}
      >
        <h2 style={{ marginTop: "2rem" }}>Add product</h2>
        <br />
        <Dropdown
          overlay={productSuggestions}
          visible={name.length > 0 && showSuggestions ? true : false}
        >
          <Input
            style={{
              width: "100%",
              marginBottom: 12,
            }}
            placeholder="Name"
            disabled={inputDisabled}
            type="text"
            value={name}
            onChange={(e) => {
              setName(e.target.value)
              setShowSuggestions(true)
            }}
          />
        </Dropdown>

        <Select
          value={category ? category : "supermarket"}
          style={{
            width: "100%",
            marginBottom: 12,
          }}
          onChange={(val) => setCategory(val)}
        >
          <Option value="supermarket">Supermarket</Option>
          <Option value="computing">Computing</Option>
          <Option value="electronics"> Electronics</Option>
          <Option value="fashion">Fashion</Option>
          <Option value="sporting">Sporting</Option>
        </Select>

        <Input
          style={{
            width: "100%",
            marginBottom: 12,
          }}
          placeholder="Price"
          type="number"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
        />
      </Modal>

      <Modal
        visible={modalVisible}
        okText="Save"
        cancelText="Back"
        onOk={handleEditPrice}
        onCancel={() => {
          setModalVisible(false)
          setId("")
        }}
      >
        <h2 style={{ marginTop: "2rem" }}>Edit product price</h2>
        <br />

        <Input
          style={{
            width: "100%",
          }}
          placeholder="New price"
          type="number"
          value={newPrice}
          onChange={(e) => setNewPrice(e.target.value)}
        />
      </Modal>
    </div>
  )
}
