import {
  Button as ButtonAntd,
  message,
  Divider,
  Form,
  Modal,
  Typography,
  Input,
} from "antd"

import { useRouter } from "next/router"
import { useMutation, gql, useLazyQuery } from "@apollo/client"

import { useState } from "react"

const GET_SHOPNAMES = gql`
  query ShopMany {
    shopMany {
      name
      password
      email
      _id
    }
  }
`

export default function Login() {
  const [
    _getShops,
    { loading: loadingShops, error: errorShops, data: dataShops },
  ] = useLazyQuery(GET_SHOPNAMES)

  const router = useRouter()

  const [loading, setLoading] = useState(false)
  const [phoneNumber, setPhoneNumber] = useState("")
  const [shopEmail, setShopEmail] = useState("")
  const [password, setPassword] = useState("")

  const [modalVisible, setModalVisible] = useState(false)

  const handleLoginShop = () => {
    if (localStorage.getItem("shopId") == null) {
      setModalVisible(true)
    } else {
      router.push("/shopConsole")
    }
  }

  const handleSetLocalStorage = () => {
    if (shopEmail == "" || password == "") {
      message.error("Missing fields")
    } else {
      _getShops().then((payload) => {
        console.log(payload.data)

        let shop = payload.data.shopMany.filter(
          (shop) => shop.email == shopEmail && shop.password == password
        )[0]

        console.log(shop)

        if (shop) {
          localStorage.setItem("shopId", shop._id)
          message.success(
            `Welcome back. Heading to '${shop.name}' shop console`
          )
          setModalVisible(false)
          setShopEmail("")
          setPassword("")
          router.push("/shopConsole")
        } else {
          message.error("Invalid credentials")
        }
      })
    }
  }

  return (
    <div>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="70"
        height="70"
        viewBox="0 0 24 24"
        style={{
          position: "absolute",
          top: "10%",
          left: "50%",
          transform: "translateX(-50%)",
          fill: "#3F9B42",
        }}
      >
        <path d="M19.391 17.097c.351.113.526.498.385.833-.135.319-.495.482-.827.374-2.617-.855-4.357-2.074-5.285-3.693-.177-.308-.068-.7.243-.875.311-.175.707-.067.883.241.77 1.341 2.285 2.372 4.601 3.12zm-8.59-.611c-.849.491-2.271 1.315-5.227 2.186-.41.121-.597.591-.382.956.124.229.422.372.717.285 2.703-.793 4.203-1.557 5.142-2.087.933-.526 1.02-.535 1.904.11.856.626 2.31 1.537 4.894 2.477.296.107.611-.025.747-.249.229-.35.071-.821-.324-.965-3.083-1.124-4.426-2.186-5.094-2.715-.866-.685-1.156-.705-2.377.002zm-.263 3.068c-.638.328-1.6.822-3.251 1.393-.215.074-.375.252-.425.472-.108.475.343.915.79.762 1.772-.607 2.803-1.138 3.482-1.487.518-.267.835-.321 1.429-.001.752.404 1.938 1.042 3.593 1.705.468.188.945-.226.856-.714-.04-.221-.191-.405-.401-.49-1.578-.635-2.711-1.244-3.431-1.631-1.133-.609-1.265-.717-2.642-.009zm-.694 3.25c-.228.106-.369.337-.358.586.017.387.368.61.693.61.091 0 .181-.018.26-.055 1.7-.792 1.11-.84 3.027.005.076.034.161.05.25.05.32 0 .677-.212.698-.603.014-.256-.134-.493-.37-.597-2.496-1.095-1.827-1.096-4.2.004zm2.354-14.206c.139-.327-.017-.704-.346-.841-.33-.137-.709.016-.848.343-1.058 2.498-3.731 4.424-7.253 5.273-.335.081-.551.404-.495.741.06.361.417.598.78.511 3.469-.833 6.784-2.773 8.162-6.027zm.647 4.136c.822-.932 1.476-1.965 1.944-3.071.47-1.111.389-2.231-.228-3.153-.646-.964-1.815-1.563-3.051-1.563-.698 0-1.37.192-1.944.555-.627.398-1.122.995-1.432 1.726-.647 1.527-2.344 2.755-4.654 3.411-.288.082-.485.345-.48.643.007.416.41.711.813.597 2.7-.766 4.714-2.263 5.515-4.153.444-1.05 1.322-1.494 2.182-1.494 1.428 0 2.81 1.224 2.085 2.935-1.529 3.612-5.11 5.937-9.157 6.958-.178.045-.33.162-.417.323-.087.161-.104.351-.044.523.107.31.436.485.755.405 1.984-.499 3.819-1.28 5.372-2.294 1.048-.685 1.97-1.474 2.741-2.348zm-5.819-6.2c.293-.501.571-.974 1.049-1.414 1.13-1.041 2.662-1.543 4.204-1.379 1.453.155 2.734.882 3.514 1.993 1.08 1.539.809 3.067.547 4.544-.225 1.263-.456 2.569.263 3.712.543.863 1.571 1.518 3.177 2.006.339.103.699-.098.785-.439.087-.345-.113-.696-.456-.802-1.246-.382-2.04-.86-2.407-1.444-.457-.726-.285-1.691-.087-2.81.279-1.571.625-3.526-.759-5.5-.994-1.417-2.611-2.341-4.438-2.536-1.914-.205-3.818.42-5.223 1.715-.62.569-.975 1.174-1.288 1.708-.493.84-.909 1.546-2.312 2.005-.222.073-.398.261-.435.54-.06.46.386.827.832.682 1.879-.614 2.464-1.611 3.034-2.581zm-2.06-1.69l.387-.572c1.549-2.217 4.286-3.304 7.323-2.909 2.886.376 5.256 2.014 6.037 4.173.692 1.914.419 3.459.199 4.701-.19 1.072-.354 1.999.22 2.742.233.302.565.535 1.021.71.38.146.796-.105.842-.505.035-.302-.137-.59-.42-.707-.195-.08-.334-.173-.416-.279-.229-.295-.116-.933.027-1.739.233-1.318.553-3.123-.255-5.357-1.13-3.123-4.746-5.102-8.454-5.102-2.466 0-4.86.882-6.553 2.746-.427.478-.69.823-.945 1.409-.167.382-.102.658.178.848.275.187.627.115.809-.159z" />
      </svg>

      <p
        style={{
          width: "100%",
          textAlign: "center",
          fontSize: "0.9rem",
          position: "absolute",
          top: "calc(10% + 100px)",
        }}
      >
        It&apos;s time we knew who you are!
      </p>
      <div
        style={{
          padding: "0px 48px",
          position: "absolute",
          top: "calc(10% + 180px)",
        }}
      >
        <p
          style={{
            color: "green",
            letterSpacing: "0.3rem",
            fontFamily: "Metropolis-Bold",
            marginBottom: 48,
            width: "100%",
            textAlign: "center",
          }}
        >
          DEAL SASA
        </p>
        <Form>
          <Form.Item label="Phone number" name="telephone">
            <div style={{ padding: "4px 0px" }}>
              <input
                style={{
                  background: "#f1f1f1",
                  width: "100%",
                  padding: "14px",
                  border: "none",
                  outline: "none",
                  borderRadius: 12,
                  color: "#3F9B42",
                  fontWeight: "bold",
                  fontSize: "1.1rem",
                }}
                type="number"
                value={phoneNumber}
                onChange={(e) => setPhoneNumber(e.target.value)}
              />
              <br />
              <ButtonAntd
                loading={false}
                size="large"
                onClick={() =>
                  message.info("Login feature still a work in progress")
                }
                style={{
                  fontFamily: "Metropolis-Regular",
                  display: "block",
                  fontSize: "1rem",
                  backgroundColor: "#3F9B42",
                  fontWeight: "900",
                  textTransform: "uppercase",
                  color: "#fff",
                  border: "none",
                  borderRadius: "8px",
                  width: "100%",
                  height: 36,
                  marginTop: "2rem",
                }}
                full
              >
                Log in
              </ButtonAntd>
            </div>
          </Form.Item>
        </Form>
        <Divider orientation="center">or</Divider>

        <ButtonAntd
          style={{
            width: "90%",
            marginTop: "3rem",
            marginLeft: "5%",
            height: "3.5rem",
            borderRadius: 12,
            fontFamily: "Metropolis-Bold",
          }}
          size="large"
          onClick={() => message.info("Login feature still a work in progress")}
          loading={false}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0px"
            y="0px"
            width="24"
            height="24"
            viewBox="0 0 48 48"
            style={{ fill: "#000000", marginRight: 12, marginBottom: -6 }}
          >
            <path
              fill="#FFC107"
              d="M43.611,20.083H42V20H24v8h11.303c-1.649,4.657-6.08,8-11.303,8c-6.627,0-12-5.373-12-12c0-6.627,5.373-12,12-12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C12.955,4,4,12.955,4,24c0,11.045,8.955,20,20,20c11.045,0,20-8.955,20-20C44,22.659,43.862,21.35,43.611,20.083z"
            ></path>
            <path
              fill="#FF3D00"
              d="M6.306,14.691l6.571,4.819C14.655,15.108,18.961,12,24,12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C16.318,4,9.656,8.337,6.306,14.691z"
            ></path>
            <path
              fill="#4CAF50"
              d="M24,44c5.166,0,9.86-1.977,13.409-5.192l-6.19-5.238C29.211,35.091,26.715,36,24,36c-5.202,0-9.619-3.317-11.283-7.946l-6.522,5.025C9.505,39.556,16.227,44,24,44z"
            ></path>
            <path
              fill="#1976D2"
              d="M43.611,20.083H42V20H24v8h11.303c-0.792,2.237-2.231,4.166-4.087,5.571c0.001-0.001,0.002-0.001,0.003-0.002l6.19,5.238C36.971,39.205,44,34,44,24C44,22.659,43.862,21.35,43.611,20.083z"
            ></path>
          </svg>
          Sign in with Google
        </ButtonAntd>

        <div style={{ display: "flex", width: "90%", margin: "32px auto" }}>
          <ButtonAntd type="link" onClick={() => router.push("/registerShop")}>
            Register shop
          </ButtonAntd>
          <span style={{ margin: "0px 6px", lineHeight: "32px" }}>|</span>
          <ButtonAntd type="link" onClick={handleLoginShop}>
            Shop console
          </ButtonAntd>
        </div>

        <Modal
          visible={modalVisible}
          okText="Login"
          cancelText="Back"
          onOk={handleSetLocalStorage}
          onCancel={() => setModalVisible(false)}
        >
          <h2 style={{ marginTop: "2rem" }}>Log in to your shop</h2>
          <br />

          <Input
            style={{
              width: "100%",
              marginBottom: 12,
            }}
            placeholder="Shop email"
            type="text"
            value={shopEmail}
            onChange={(e) => setShopEmail(e.target.value)}
          />

          <Input.Password
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            placeholder="Password"
            style={{
              marginBottom: 12,
              width: "100%",
            }}
          />

          <br />
        </Modal>
      </div>
    </div>
  )
}
