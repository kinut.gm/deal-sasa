import { Container, Product, Spin, Flex, Badge, Page } from "../../components";
import { message } from "antd";
import { ShoppingOutlined } from "@ant-design/icons";
import { useState, useEffect, useRef } from "react";
import { db } from "../../firebase/clientApp";
import DB_Services from "../../firebase/services";

import { useRouter } from "next/router";
import {
  query,
  collection,
  orderBy,
  limit,
  getDocs,
  where,
  startAfter,
  doc,
} from "firebase/firestore";
import { useDocument } from "react-firebase-hooks/firestore";
import { useAuth } from "../../context";

export default function Group() {
  const { authUser, loading } = useAuth();

  const [id, setId] = useState(null);
  const [uda, setUDA] = useState(false);
  const [prods, setProds] = useState([]);
  const [lastKey, setLastKey] = useState("");
  const [loadingMore, setLoadingMore] = useState(false);

  const scrollDiv = useRef();

  const router = useRouter();

  const { category } = router.query;

  const [user, userLoading, userError] = useDocument(
    doc(db, "users", authUser ? authUser.uid : "unfindalble"),
    {}
  );

  const getProducts = async (doc) => {
    setLoadingMore(true);

    let data = query(
      collection(db, "products"),
      orderBy("added", "desc"),
      where(
        "category",
        "==",
        typeof category != "undefined" ? category : "snacks"
      ),
      startAfter(doc),
      limit(15)
    );

    const snapShot = await getDocs(data);

    setLastKey(snapShot.docs[snapShot.docs.length - 1]);

    let _data_ = [];

    snapShot.docs.forEach((doc) => {
      _data_[_data_.length] = { id: doc.id, data: doc.data() };
    });

    setProds(prods.concat(_data_));
  };

  const fetchMore = () => {
    if (
      scrollDiv.current.offsetHeight + scrollDiv.current.scrollTop >=
      scrollDiv.current.scrollHeight
    ) {
      getProducts(lastKey).then(() => {
        setLoadingMore(false);
      });
    }
  };

  useEffect(() => {
    if (user && typeof user.data() !== "undefined") {
      setUDA(true);
    }
  }, [user]);

  useEffect(async () => {
    let data = query(
      collection(db, "products"),
      orderBy("added", "desc"),
      where(
        "category",
        "==",
        typeof category != "undefined" ? category : "snacks"
      ),
      limit(15)
    );

    const snapShot = await getDocs(data);

    let _data_ = [];

    snapShot.docs.forEach((doc) => {
      _data_[_data_.length] = { id: doc.id, data: doc.data() };
    });

    setLastKey(snapShot.docs[snapShot.docs.length - 1]);
    setProds(_data_);
  }, []);

  if (prods.length > 0 && uda) {
    console.log(prods);
  }

  const addToCart = (product) => {
    if (typeof authUser.uid == "undefined") {
      router.push("/login");
    } else {
      setId(product.id);

      DB_Services.addToCart({
        product: {
          id: product.id,
          name: product.data.name,
          image: product.data.image,
          price:
            DB_Services.verifyOnOffer(
              product.data.start_date,
              product.data.end_date
            ) && product.data.offer_price
              ? product.data.offer_price
              : product.data.price,
        },
        user: authUser.uid,
      })
        .then(() => {
          message.success(`'${product.data.name}' successfully added to cart`);
          setId(null);
        })
        .catch((err) => {
          message.error("Failed");
          setId(null);
        });
    }
  };

  const increment = (id) => {
    DB_Services.incrementCartProduct({ id, user: authUser.uid }).then((val) =>
      console.log(val)
    );
  };

  const decrement = (id, name) => {
    if (
      user != undefined &&
      user.data().cart.filter((cartItem) => cartItem.product.id == id)[0]
        .quantity == 1
    ) {
      DB_Services.removeFromCart({ id, user: authUser.uid }).then((val) => {
        console.log(val);
        message.success(`'${name}' removed from cart`);
      });
    } else {
      DB_Services.decrementCartProduct({ id, user: authUser.uid }).then((val) =>
        console.log(val)
      );
    }
  };

  const removeFromCart = (id, name) => {
    DB_Services.removeFromCart({ id, user: authUser.uid }).then((val) => {
      console.log(val);
      message.success(`'${name}' removed from cart`);
    });
  };

  return (
    <Page category title={"Snacks"}>
      <Container style={{ position: "absolute", top: 56, minWidth: "95vw" }}>
        <div
          ref={scrollDiv}
          onScroll={fetchMore}
          style={{
            width: "100%",
            maxHeight: "200vh",
            overflow: "auto",
          }}
        >
          test
          <Flex scrollY>
            {prods.length > 0 &&
              uda &&
              prods.map((product) => (
                <Product
                  on_offer={
                    product.data.start_date && product.data.end_date
                      ? DB_Services.verifyOnOffer(
                          product.data.start_date,
                          product.data.end_date
                        )
                      : false
                  }
                  offer_price={product.data.offer_price}
                  key={product.id}
                  image={product.data.image}
                  loading={id == product.id ? true : false}
                  add={() => addToCart(product)}
                  rounded
                  basket={
                    user
                      .data()
                      .cart.filter(
                        (cartItem) =>
                          cartItem.product.id == product.id &&
                          cartItem.paid == false &&
                          cartItem.deliver_first == false
                      ).length > 0
                      ? true
                      : false
                  }
                  quantity={
                    user
                      .data()
                      .cart.filter(
                        (cartItem) =>
                          cartItem.product.id == product.id &&
                          cartItem.paid == false &&
                          cartItem.deliver_first == false
                      ).length > 0
                      ? user
                          .data()
                          .cart.filter(
                            (cartItem) =>
                              cartItem.product.id == product.id &&
                              cartItem.paid == false &&
                              cartItem.deliver_first == false
                          )[0].quantity
                      : 0
                  }
                  name={product.data.name}
                  price={product.data.price}
                  id={product.id}
                  remove={(id) => {
                    if (
                      user
                        .data()
                        .cart.filter(
                          (cartItem) =>
                            cartItem.product.id == id &&
                            cartItem.paid == false &&
                            cartItem.deliver_first == false
                        ).length > 0
                    ) {
                      removeFromCart(id, product.data.name);
                    }
                  }}
                  increment={(id) => increment(id, product.data.name)}
                  decrement={(id) => decrement(id, product.data.name)}
                />
              ))}
          </Flex>
          <Flex scrollY>
            {prods.length > 0 &&
              (!uda || userLoading || userError) &&
              prods.map((product) => (
                <Product
                  on_offer={
                    product.data.start_date && product.data.end_date
                      ? DB_Services.verifyOnOffer(
                          product.data.start_date,
                          product.data.end_date
                        )
                      : false
                  }
                  offer_price={product.data.offer_price}
                  key={product.id}
                  image={product.data.image}
                  loading={id == product.id ? true : false}
                  add={() => addToCart(product)}
                  rounded
                  name={product.data.name}
                  price={product.data.price}
                  id={product.id}
                />
              ))}
          </Flex>
          {/* {loadingMore && (
            <Spin
              style={{
                position: "absolute",
                bottom: "0.5rem",
                left: "50%",
                transform: "translateY(-50%)",
              }}
            />
          )} */}
        </div>
      </Container>
    </Page>
  );
}
