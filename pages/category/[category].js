import { Container, Product, Flex, Page } from "../../components"
import { message, Badge } from "antd"
import { ShoppingOutlined } from "@ant-design/icons"
import { useState, useRef } from "react"

import { useQuery, gql } from "@apollo/client"

import { useRouter } from "next/router"

const GET_PRODUCTS = gql`
  query ProductsPopulate {
    productsPopulate {
      name
      shops {
        name
        price
        id
      }
      category
      id
    }
  }
`

const GET_CART_COUNT = gql`
  query Query($filter: FilterCountCartInput) {
    cartCount(filter: $filter)
  }
`

export default function Group() {
  const scrollDiv = useRef()

  const router = useRouter()
  const [keyword, setKeyword] = useState("")
  const [highlighted, setHighlighted] = useState("")

  const { category } = router.query

  const { loading, error, data } = useQuery(GET_PRODUCTS)
  const {
    loading: loadingCount,
    error: errorCount,
    data: dataCount,
  } = useQuery(GET_CART_COUNT, {
    variables: {
      filter: {
        customerId: "62500b76af64fd2326efdb3a",
      },
    },
  })

  if (loading) return <div>Loading...</div>
  if (error) return <div>Error!</div>

  console.log(data)

  return (
    <Page
      category
      getKeyword={(val) => console.log(val)}
      title={typeof category != "undefined" ? category : "..."}
      badge={
        <button
          style={{
            height: 40,
            margin: 0,
            outline: "none",
            border: "none",
            background: "transparent",
            display: "inline",
            top: 12,
          }}
          onClick={() => {
            router.push("/basket")
          }}
        >
          <Badge color="#3F9B42" count={dataCount ? dataCount.cartCount : 0}>
            <ShoppingOutlined style={{ fontSize: "1.5rem", color: "white" }} />
          </Badge>
        </button>
      }
    >
      <Container style={{ position: "absolute", top: 56, minWidth: "95vw" }}>
        <div
          ref={scrollDiv}
          onScroll={null}
          style={{
            width: "100%",
            maxHeight: "200vh",
            overflow: "auto",
          }}
        >
          <Flex scrollY>
            {data.productsPopulate
              .filter((product) => product.category == category)
              .map((product) => (
                <Product
                  on_offer={false}
                  offer_price={0}
                  key={product.id}
                  image={null}
                  loading={false}
                  add={() => {
                    console.log("add to cart")
                  }}
                  rounded
                  highlighted={highlighted == product.id ? true : false}
                  basket={false}
                  quantity={0}
                  shop={product.shops[product.shops.length - 1].name}
                  price={product.shops[product.shops.length - 1].price}
                  name={product.name}
                  id={product.id}
                  product={product}
                  remove={(id) => {
                    console.log("remove", id)
                  }}
                  increment={(id) => console.log("increment", id)}
                  decrement={(id) => console.log("decrement", id)}
                />
              ))}
          </Flex>
        </div>
      </Container>
    </Page>
  )
}
