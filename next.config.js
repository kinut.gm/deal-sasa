const withAntdLess = require("next-plugin-antd-less")
const withPlugins = require("next-compose-plugins")

// optional next.js configuration
const nextConfig = {
  env: {
    GOOGLE_ID:
      "585123557020-8a9rboqg23d81ncjqthe71v3us327bsk.apps.googleusercontent.com",
    GOOGLE_SECRET: "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",
  },
}

module.exports = withPlugins(
  [
    // another plugin with a configuration
    [
      withAntdLess,
      {
        // optional: you can modify antd less variables directly here
        modifyVars: { "@primary-color": "#3F9B42" },
        // Or better still you can specify a path to a file
        // lessVarsFilePath: "./styles/variables.less",
        // optional
        lessVarsFilePathAppendToEndOfContent: false,
        // optional https://github.com/webpack-contrib/css-loader#object
        cssLoaderOptions: {},
      },
    ],
  ],
  nextConfig
)
