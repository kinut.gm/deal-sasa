export default function Title({ text, yellow, center, style, medium, large }) {
  return (
    <h3
      style={{
        fontSize: medium ? "0.8rem" : large ? "1.5rem" : "1.2rem",
        color: yellow ? "#E5E055" : "#3F9B42",
        display: "block",
        width: "100%",
        textAlign: center ? "center" : "left",
        margin: "0.4rem 0rem",
        letterSpacing: "-0.07rem",

        ...style,
      }}
    >
      {text}
    </h3>
  )
}
