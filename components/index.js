import Product from "./Product";
import Button from "./Button";
import Title from "./Title";
import Search from "./Search";
import Category from "./Category";
import Text from "./Text";
import Flex from "./Flex";
import Page from "./Page";
import Container from "./Container";
import SkeletonCustom from "./Skeleton";

module.exports = {
  Container,
  Product,
  Button,
  Title,
  Search,
  Category,
  Text,
  Flex,
  Page,
  SkeletonCustom
};
