import Title from "./Title"
import Button from "./Button"
import { useRouter } from "next/router"
// import Image from "next/image";
import {
  Image,
  Button as ButtonAntd,
  Skeleton,
  Tag,
  Typography,
  Tooltip,
  Modal,
  Spin,
  Col,
  Badge,
  message,
} from "antd"

import { PlusOutlined, MinusOutlined, CloseOutlined } from "@ant-design/icons"
import { useState } from "react"

import { useMutation, gql, useQuery } from "@apollo/client"

const { Paragraph } = Typography

const ADD_TO_CART = gql`
  mutation Mutation($record: CreateOneCartInput!) {
    cartCreateOne(record: $record) {
      record {
        shop {
          name
          _id
        }
        customer {
          name
        }
        product {
          name
          _id
        }
        quantity
      }
    }
  }
`

export default function Product({
  loading,
  add,
  id,
  name,
  price,
  quantity,
  image,
  remove,
  increment,
  decrement,
  basket,
  rounded,
  shop,
  on_offer,
  offer_price,
  old,
  highlighted,
  product,
}) {
  const [modalVisible, setModalVisible] = useState(false)
  const [display, setDisplay] = useState(false)
  const router = useRouter()

  const getAverage = () => {
    let tot = 0

    product.shops.forEach((shop) => {
      tot = tot + shop.price
    })

    return (tot / product.shops.length).toFixed(2)
  }

  const calculateRate = (index) => {
    let tot = 0

    product.shops.forEach((shop) => {
      tot = tot + shop.price
    })

    let average = (tot / product.shops.length).toFixed(2)

    let rate = (
      ((product.shops[index].price - average) / average) *
      100
    ).toFixed(2)

    if (rate > 0) {
      return `+${rate}`
    } else {
      return rate
    }
  }

  const [
    _addToCart,
    { data: dataMutation, loading: loadingMutation, error: errorMutation },
  ] = useMutation(ADD_TO_CART)

  return (
    <section
      id={id}
      style={{
        width: "calc(50vw - 32px)",
        maxWidth: 200,
        margin: "0px 16px 16px 5px",
        background: old || highlighted ? "rgba(229,224,85,0.2)" : "#f1f1f1",
        borderRadius: rounded ? "8px" : "0px",
        position: "relative",
      }}
    >
      {basket ? (
        <Button
          onClick={() => remove(id)}
          label={
            <img
              src={"/cross.svg"}
              style={{ height: 32, width: 32, objectFit: "cover", zIndex: 3 }}
            />
          }
          style={{
            zIndex: 2,
            position: "absolute",
            top: 3,
            right: 12,
            height: 32,
            background: "transparent",
          }}
          width={32}
        />
      ) : null}

      <div style={{ padding: 12 }}>
        {image ? (
          <Image
            style={{
              width: `calc(50vw - 56px)`,
              height: `calc(50vw - 56px)`,
              objectFit: "cover",
              marginBottom: 12,
              width: "100%",
              maxWidth: "calc(200px - 24px)",
              maxHeight: "calc(200px - 24px)",
            }}
            src={image}
            loading="eager"
          />
        ) : (
          <Skeleton.Button
            style={{
              width: `calc(50vw - 56px)`,
              height: `calc(50vw - 56px)`,
              marginBottom: 12,
            }}
            active
          />
        )}

        <Tooltip title={name} color="#3F9B42" trigger="click">
          <Paragraph
            ellipsis={{ rows: 2, expandable: false }}
            style={{
              color: "#3F9B42",
              width: `calc(50vw - 60px)`,
              maxWidth: "calc(200px - 24px)",
              height: 45,
              margin: 0,
            }}
          >
            {name}
          </Paragraph>
        </Tooltip>

        <Paragraph
          ellipsis={{ rows: 2, expandable: false }}
          style={{
            color: "#909090",
            width: `calc(50vw - 60px)`,
            maxWidth: "calc(200px - 24px)",
            fontSize: "0.7rem",
            fontWeight: "400",
            margin: 0,
          }}
        >
          {shop}
        </Paragraph>

        {on_offer && offer_price && (
          <span
            style={{
              display: "flex",
              margin: "0.4rem 0rem",
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <Title text={`Ksh. ${offer_price}`} style={{ margin: 0 }} />
            <h3
              style={{
                textDecoration: "line-through",
                color: "gray",
                margin: 0,
                lineHeight: 2,
                fontSize: "0.8rem",
                fontWeight: "400",
              }}
            >
              {price}
            </h3>
          </span>
        )}

        {!on_offer && <Title text={`Ksh. ${price}`} yellow={old} />}

        {old && (
          <div
            style={{ display: "flex", justifyContent: "center", width: "100%" }}
          >
            <Tag color="#E5E055">{quantity} Products</Tag>
            {/* <Title
              style={{ lineHeight: 1, color: "#3F9B42", textAlign: "center" }}
              text={quantity}
            /> */}
          </div>
        )}

        {basket && (
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <div style={{ width: 36 }}>
              <Button
                rounded
                label={<MinusOutlined />}
                style={{ height: 36 }}
                full
                onClick={() => decrement(id)}
              />
            </div>
            <div>
              <Title
                style={{ lineHeight: 1, color: "#707070" }}
                text={quantity}
              />
            </div>
            <div style={{ width: 36 }}>
              <Button
                rounded
                label={<PlusOutlined />}
                style={{ height: 36 }}
                full
                onClick={() => increment(id)}
              />
            </div>
          </div>
        )}

        {old ? null : basket ? null : (
          <div style={{ display: "flex" }}>
            <ButtonAntd
              loading={loading}
              onClick={() => {
                _addToCart({
                  variables: {
                    record: {
                      productId: product.id,
                      shopId: product.shops[product.shops.length - 1].id,
                      customerId: "62500b76af64fd2326efdb3a",
                      quantity: 1,
                    },
                  },
                }).then(() => {
                  message.success(
                    `'${product.name}' from '${
                      product.shops[product.shops.length - 1].name
                    }' successfully added to cart`
                  )

                  router.reload(window.location.pathname)
                })
              }}
              style={{
                fontFamily: "Metropolis-Regular",
                display: "block",
                fontSize: "1rem",

                backgroundColor: "#3F9B42",
                fontWeight: "900",
                textTransform: "uppercase",
                color: "#fff",
                border: "none",
                borderRadius: "8px",
                width: "100%",
                height: 36,
              }}
              full
            >
              {loading ? "Adding" : "Add"}
            </ButtonAntd>
            <button
              onClick={() => {
                setModalVisible(true)
                setTimeout(() => {
                  setDisplay(true)
                }, 1000)
              }}
              style={{
                height: 36,
                width: 36,
                background: "transparent",
                border: "none",
                marginLeft: 4,
                outline: "none",
              }}
            >
              <img src={"/compare.png"} style={{ height: 24, width: 24 }} />
            </button>
          </div>
        )}
      </div>

      <Modal
        visible={modalVisible}
        footer={null}
        onCancel={() => setModalVisible(false)}
      >
        <h2>Compare</h2>
        <p style={{ color: "#707070" }}>
          Average product price is about{" "}
          <span style={{ color: "#3F9B42", fontWeight: "bold" }}>
            Ksh. {getAverage()}
          </span>
        </p>
        <br />

        {display &&
          product.shops.map((shop, index) => (
            <div style={{ display: "flex", padding: "5px 0px" }} key={shop}>
              <Col span={7}>
                <Paragraph
                  ellipsis={{ rows: 1, expandable: false }}
                  style={{
                    fontWeight: "bold",
                    width: "calc(100% - 8px)",
                    height: 45,
                    margin: 0,
                  }}
                >
                  {shop.name}
                </Paragraph>
              </Col>
              <Col span={6}>
                <p style={{ color: "#808080" }}>Ksh. {shop.price}</p>
              </Col>
              <Col span={4}>
                <span>
                  {" "}
                  <Tag
                    color="blue"
                    style={{
                      zIndex: 2,
                      position: "absolute",
                      top: 0,
                      left: 0,
                    }}
                  >
                    {calculateRate(index)}%
                  </Tag>{" "}
                </span>
              </Col>
              <Col span={6} offset={1}>
                <ButtonAntd
                  type="secondary"
                  style={{ color: "#3F9B42" }}
                  onClick={() =>
                    _addToCart({
                      variables: {
                        record: {
                          productId: product.id,
                          shopId: shop.id,
                          customerId: "62500b76af64fd2326efdb3a",
                          quantity: 1,
                        },
                      },
                    }).then(() => {
                      message.success(
                        `'${product.name}' from '${shop.name}' successfully added to cart`
                      )
                      setModalVisible(false)
                      router.reload(window.location.pathname)
                    })
                  }
                >
                  Buy here
                </ButtonAntd>
              </Col>
            </div>
          ))}
        <br />

        {!display && (
          <Spin
            color="#3F9B42"
            tip="Finding deals..."
            style={{ display: "block", margin: "0 auto" }}
          ></Spin>
        )}
      </Modal>
    </section>
  )
}
